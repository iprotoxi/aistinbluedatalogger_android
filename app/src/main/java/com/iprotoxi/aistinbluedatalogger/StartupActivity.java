package com.iprotoxi.aistinbluedatalogger;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class StartupActivity extends AppCompatActivity {

    private final static String TAG = StartupActivity.class.getSimpleName();

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner mBluetoothLeScanner;
    private ScanSettings mScanSettings;
    private List<ScanFilter> mScanFilters;

    private final int REQUEST_ENABLE_BT = 1;
    private final int REQUEST_ENABLE_GPS = 1;
    private static final long SCANNING_TIME = 10000; //circa 10 seconds

    private boolean mScanning = false;
    private Handler mHandler;

    private DeviceListAdapter mDeviceListAdapter;
    private ListView mListView;

    private boolean DEBUG = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        //show app logo on action bar
        try {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setLogo(R.mipmap.aistin_eye);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
        } catch (Exception ex) {
            Log.w(TAG, ex);
        }

        mHandler = new Handler();

        final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();

        if (mBluetoothAdapter == null) {
            Toast.makeText(this, R.string.bluetooth_not_supported, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        mListView = (ListView) findViewById(R.id.listView_devices);
        mDeviceListAdapter = new DeviceListAdapter();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mListView.setAdapter(mDeviceListAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (DEBUG) Log.d(TAG, "onItemClick");
                final BluetoothDevice device = mDeviceListAdapter.getDevice(position);
                listItemClickAction(device);
            }
        });

        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled() )
        {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            Intent enableLocationIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(enableLocationIntent, REQUEST_ENABLE_GPS);
        } else {
            if (Build.VERSION.SDK_INT >= 21 ) {
                mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
                mScanSettings = new ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_BALANCED).build();
                mScanFilters = new ArrayList<>();
            }
            if (isLocationEnabled(this));
            /* TODO: Android 6.0 run-time permission requests
            If the device is running Android 6.0 (API level 23) or higher, and the app's
            targetSdkVersion is 23 or higher, the app requests permissions from the user at run-time.
            The user can revoke the permissions at any time, so the app needs to check whether it has
            the permissions every time it runs.
            (http://developer.android.com/guide/topics/security/permissions.html)
            BUBBLEGUM FIX: Dropped APP's target sdk from 23 to 22
            */
            //scanDevices(true); //auto-scanning right when the activity is opened
        }
         scanDevices(true); //auto-scanning right when the activity is opened
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()){
            scanDevices(false); //auto-scanning should be stopped when the activity is paused
        }
        //mListView.setAdapter(null);
        mDeviceListAdapter.clear();
        mDeviceListAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy(){
        scanDevices(false);
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        // User chose not to enable GPS location services.
        if (requestCode == REQUEST_ENABLE_GPS && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.startup, menu);
        if (mScanning) {
            menu.findItem(R.id.menu_item_bt_on).setVisible(true);
            menu.findItem(R.id.menu_item_bt_off).setVisible(false);
        } else {
            menu.findItem(R.id.menu_item_bt_on).setVisible(false);
            menu.findItem(R.id.menu_item_bt_off).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.menu_item_bt_on:
                Toast.makeText(getApplicationContext(), "Stopped scanning", Toast.LENGTH_SHORT).show();
                scanDevices(false);
                invalidateOptionsMenu();
                break;
            case R.id.menu_item_bt_off:
                Toast.makeText(getApplicationContext(), "Scanning for devices...", Toast.LENGTH_LONG).show();
                mDeviceListAdapter.clear();
                mDeviceListAdapter.notifyDataSetChanged();
                scanDevices(true);
                invalidateOptionsMenu();
                break;
            default:
                Log.w(TAG, "unrecognized menu item action");
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void scanDevices(final boolean enable) {
        if (enable) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mScanning = false;
                    mBluetoothLeScanner.stopScan(mScanCallback);
                    invalidateOptionsMenu();
                }
            }, SCANNING_TIME);
            mBluetoothLeScanner.startScan(mScanFilters, mScanSettings, mScanCallback);
            mScanning = true;
        }
        else {
            mBluetoothLeScanner.stopScan(mScanCallback);
            mScanning = false;
        }
        invalidateOptionsMenu();
    }

    private class DeviceListAdapter extends BaseAdapter {
        private final ArrayList<BluetoothDevice> mBleDevices;
        private final LayoutInflater mDeviceListInflater;

        public DeviceListAdapter() {
            super();
            mBleDevices = new ArrayList<>();
            mDeviceListInflater = StartupActivity.this.getLayoutInflater();
        }

        public void addDevice(BluetoothDevice device) {
            if (!mBleDevices.contains(device)) {
                mBleDevices.add(device);
            }
        }

        public BluetoothDevice getDevice(int position) {
            return mBleDevices.get(position);
        }

        public void clear() {
            mBleDevices.clear();
        }

        @Override
        public int getCount() { return mBleDevices.size(); }

        @Override
        public Object getItem(int i) { return mBleDevices.get(i); }

        @Override
        public long getItemId(int i) { return i; }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup ){
            ViewHolder viewHolder;

            if (view == null) {
                view = mDeviceListInflater.inflate(R.layout.list_item_device, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
                view.setTag(viewHolder);
            }
            else {
                viewHolder = (ViewHolder) view.getTag();
            }

            BluetoothDevice device = mBleDevices.get(i);
            final String deviceName = device.getName();
            if (deviceName != null && deviceName.length() > 0) {
                viewHolder.deviceName.setText(deviceName);
            }
            else {
                viewHolder.deviceName.setText(R.string.unkonwn_device);
            }
            viewHolder.deviceAddress.setText(device.getAddress());

            return view;
        }
    }

    static class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
    }

    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            //super.onScanResult(callbackType, result);
            if (DEBUG) Log.d(TAG, "mScanCallback callbackType: " + String.valueOf(callbackType) + ", ScanResult: " + result.toString());
            mDeviceListAdapter.addDevice(result.getDevice());
            mDeviceListAdapter.notifyDataSetChanged();
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            //super.onBatchScanResults(results);
            for (ScanResult i : results) {
                if (DEBUG) Log.d(TAG, "mScanCallback onBatchScanResults: " + i.toString());
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            //super.onScanFailed(errorCode);
            Log.e(TAG, "mScanCallback errorCode: " + errorCode);
        }
    };

    @Deprecated
    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (DEBUG) Log.d(TAG, "mLeScanCallback");
                    mDeviceListAdapter.addDevice(device);
                    mDeviceListAdapter.notifyDataSetChanged();
                }
            });
        }
    };

    private void listItemClickAction(BluetoothDevice device) {
        if (DEBUG) Log.d(TAG, "listItemClickAction");
        if (device == null) return;
        if (mScanning) scanDevices(false);
        final Intent intent = new Intent(this, LoggerActivity.class);
        intent.putExtra(LoggerActivity.CONNECTED_DEVICE_NAME, device.getName());
        intent.putExtra(LoggerActivity.CONNECTED_DEVICE_ADDRESS, device.getAddress());
        startActivity(intent);
    }

    private static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;
        try {
            locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return locationMode != Settings.Secure.LOCATION_MODE_OFF;
    }
}

