package com.iprotoxi.aistinbluedatalogger;

import android.app.NotificationManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

public class BlueService extends Service {

    private final static String TAG = BlueService.class.getSimpleName();

    /* GATT Services and characteristics supported by iProtoXi Aistin Blue */

    private final static UUID DEVICE_INFORMATION_SERVICE_UUID = UUID.fromString("0000180A-0000-1000-8000-00805F9B43FB");
    private final static UUID MANUFACTURER_NAME_UUID = UUID.fromString("00002A00-0000-1000-8000-00805F9B43FB");

    private final static UUID BATTERY_SERVICE_UUID = UUID.fromString("0000180F-0000-1000-8000-00805F9B43FB");
    private final static UUID BATTERY_LEVEL_UUID = UUID.fromString("00002A19-0000-1000-8000-00805F9B43FB");

    private final static UUID NRF_UART_SERVICE_UUID = UUID.fromString("6E400001-B5A3-F393-E0A9-E50E24DCCA9E");
    private final static UUID NRF_UART_RX_UUID = UUID.fromString("6E400002-B5A3-F393-E0A9-E50E24DCCA9E"); //write without response
    private final static UUID NRF_UART_TX_UUID = UUID.fromString("6E400003-B5A3-F393-E0A9-E50E24DCCA9E"); //notify

    private final static UUID DFU_SERVICE = UUID.fromString("00001530-1212-EFDE-1523-785FEABCD123");

    //universal for Notify or Indicate types of characteristics
    private final static UUID CLIENT_CHARACTERISTIC_CONFIGURATION_DESCRIPTOR = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    public final static String INTENT_ACTION_GATT_CONNECTED = "INTENT_ACTION_GATT_CONNECTED";
    public final static String INTENT_ACTION_GATT_DISCONNECTED = "INTENT_ACTION_GATT_DISCONNECTED";
    public final static String INTENT_ACTION_GATT_SERVICES_DISCOVERED = "INTENT_ACTION_GATT_SERVICES_DISCOVERED";
    public final static String INTENT_ACTION_DATA_AVAILABLE = "INTENT_ACTION_DATA_AVAILABLE";
    public final static String INTENT_ACTION_EXTRA_DATA= "INTENT_ACTION_EXTRA_DATA";
    public final static String INTENT_DEVICE_DOES_NOT_SUPPORT_UART= "INTENT_DEVICE_DOES_NOT_SUPPORT_UART";

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothGatt mBluetoothGatt;
    private String mBluetoothDeviceAddress;
    private int mConnectionState = STATE_DISCONNECTED;

    private NotificationManager mNotificationManager;

    //public static FileWriter mFileWriter;
    private static BufferedWriter mBufferedWriter;
    private static File mFileLocation;

    private boolean DEBUG = false;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    public class LocalBinder extends Binder {
        BlueService getService() { return BlueService.this; }
    }

    private final IBinder mBinder = new LocalBinder();

    @Override
    public IBinder onBind(Intent intent) { return mBinder; }

    @Override
    public boolean onUnbind(Intent intent) {
        close();
        return super.onUnbind(intent);
    }

    /* Initializes a reference to local BT adapter */
    public boolean initialize(){
        if (DEBUG) Log.d(TAG, "initialize");
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null){
            Log.e(TAG, "Unable to obtain BluetoothAdapter");
            return false;
        }
        return true;
    }

    /* Connects to the GATT server hosted on the Blue device */
    public boolean connect(final String deviceAddress){
        if (DEBUG) Log.d(TAG, "connect");
        if (mBluetoothAdapter == null || deviceAddress == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address");
            return false;
        }

        if (mBluetoothDeviceAddress != null && deviceAddress.equals(mBluetoothDeviceAddress) && mBluetoothGatt != null ){
            if (DEBUG) Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection");
            if (mBluetoothGatt.connect()) {
                mConnectionState = STATE_CONNECTING;
                return true;
            } else {
                return false;
            }
        }

        final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(deviceAddress);
        if (device == null) {
            Log.w(TAG, "Unable to connect, device not found");
            return false;
        }

        mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        if (DEBUG) Log.d(TAG, "Trying to create a new connection");
        mBluetoothDeviceAddress = deviceAddress;
        mConnectionState = STATE_CONNECTING;

        return true;
    }

    /* Disconnects a connection or cancels a pending connection */
    public void disconnect(){
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.disconnect();
    }

    /* Closes the gatt client when device is not needed for use anymore */
    public void close() {
        if (mBluetoothGatt == null) return;
        mBluetoothDeviceAddress = null;
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }


    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback()
    {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED)
            {
                Log.i(TAG, "mGattCallback - Connected to GATT server");
                mConnectionState = STATE_CONNECTED;
                intentAction = INTENT_ACTION_GATT_CONNECTED;
                broadcastUpdate(intentAction);
                if (mBluetoothGatt != null)
                {
                    Log.i(TAG, "mGattCallback - Starting service discovery: " + mBluetoothGatt.discoverServices());
                }
            }
            else if (newState == BluetoothProfile.STATE_DISCONNECTED)
            {
                Log.i(TAG, "mGattCallback - Disconnected from GATT server");
                mConnectionState = STATE_DISCONNECTED;
                intentAction = INTENT_ACTION_GATT_DISCONNECTED;
                broadcastUpdate(intentAction);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (DEBUG) Log.d(TAG, "onServicesDiscovered");
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (mBluetoothGatt != null) {
                    Log.d(TAG, "discovered: " + gatt.getServices());
                }
                broadcastUpdate(INTENT_ACTION_GATT_SERVICES_DISCOVERED);
            } else {
                Log.w(TAG, "onServicesDiscovered: " + status);
            }
            //Auto-enable notifications from device:
            //setCharacteristicNotificationNRFUartTX(true);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (DEBUG) Log.d(TAG, "onCharacteristicRead");
            if (status == BluetoothGatt.GATT_SUCCESS){
                broadcastUpdate(INTENT_ACTION_DATA_AVAILABLE, characteristic);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            if (DEBUG) Log.d(TAG, "incoming data");
            //broadcastUpdate(INTENT_ACTION_DATA_AVAILABLE, characteristic);
            //parse and write to file
            final byte[] rawSensorData = characteristic.getValue();//  getByteArrayExtra(BlueService.INTENT_ACTION_EXTRA_DATA);
            //if (DEBUG) Log.d(TAG, "rawSensorData: " + AistinBlueBoard.ConvertByteArrayToHexString(rawSensorData));
            String message = AistinBlueBoard.ParseMessageToSignedIntStringCSV(rawSensorData);
            //FIXME: writing to file seems to cause ART to suspend all threads multiple times
            FileWriterTask task = new FileWriterTask();
            task.execute(message);
        }

    };

    private void broadcastUpdate(final String intentAction) {
        final Intent intent = new Intent(intentAction);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String intentAction, final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(intentAction);

        if (isNRFUartReadCharacteristic(characteristic)) {
            byte[] data = characteristic.getValue();
            intent.putExtra(INTENT_ACTION_EXTRA_DATA, data);
        }
        sendBroadcast(intent);
    }

    public void setCharacteristicNotificationNRFUartTX(boolean enabled) {
        Log.i(TAG, "setCharacteristicNotificationNRFUartTX enabled: " + enabled);

        BluetoothGattService nRFUartService = mBluetoothGatt.getService(NRF_UART_SERVICE_UUID);
        if (nRFUartService == null) {
            Log.w(TAG, "nRFUartService not found");
            broadcastUpdate(INTENT_DEVICE_DOES_NOT_SUPPORT_UART);
            return;
        }
        BluetoothGattCharacteristic nRFUartTXCharacteristic = nRFUartService.getCharacteristic(NRF_UART_TX_UUID);
        if (nRFUartTXCharacteristic == null) {
            Log.w(TAG, "nRFUartTXCharacteristic not found");
            broadcastUpdate(INTENT_DEVICE_DOES_NOT_SUPPORT_UART);
            return;
        }
        BluetoothGattDescriptor descriptor = nRFUartTXCharacteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIGURATION_DESCRIPTOR);
        if (enabled) {
            mBluetoothGatt.setCharacteristicNotification(nRFUartTXCharacteristic, true);
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        } else {
            mBluetoothGatt.setCharacteristicNotification(nRFUartTXCharacteristic, false);
            descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
        }
        mBluetoothGatt.writeDescriptor(descriptor);
    }

    private boolean isDeviceInformationService (final BluetoothGattService service) {
        return service != null && DEVICE_INFORMATION_SERVICE_UUID.equals(service.getUuid());
    }
    private boolean isManufacturerNameCharacteristic (final BluetoothGattCharacteristic characteristic) {
        return characteristic != null && MANUFACTURER_NAME_UUID.equals(characteristic.getUuid());
    }
    private boolean isBatteryService (final BluetoothGattService service) {
        return service != null && BATTERY_SERVICE_UUID.equals(service.getUuid());
    }
    private boolean isBatteryLevelCharacteristic (final BluetoothGattCharacteristic characteristic) {
        return characteristic != null && BATTERY_LEVEL_UUID.equals(characteristic.getUuid());
    }
    private boolean isNRFUartService (final BluetoothGattService service) {
        return service != null && NRF_UART_SERVICE_UUID.equals(service.getUuid());
    }
    private boolean isNRFUartWriteCharacteristic (final BluetoothGattCharacteristic characteristic) {
        return characteristic != null && NRF_UART_RX_UUID.equals(characteristic.getUuid());
    }
    private boolean isNRFUartReadCharacteristic(final BluetoothGattCharacteristic characteristic) {
        return characteristic != null && NRF_UART_TX_UUID.equals(characteristic.getUuid());
    }

    //Async task for writing to file
    private class FileWriterTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... params) {
            appendFile(params[0] + "\n");
            finish();
            return null;
        }

        private Context getBaseContext() {
            // TODO Auto-generated method stub
            return null;
        }

        private void finish() {
            // TODO Auto-generated method stub
        }
    }

    public static void setupBufferedWriter(String mFileName, File directory){
        mFileLocation = new File(directory, mFileName);
    }

    public static void createBufferedWriter(){
        try {
            mBufferedWriter = new BufferedWriter(new FileWriter(mFileLocation, true));
        } catch (IOException e) {
            Log.e(TAG, "IO Exception while creating a new BufferedWriter");
            e.printStackTrace();
        }
    }

    public static void appendFile(String line) {
        try {
            mBufferedWriter.append(line);
        } catch (IOException e) {
            Log.w(TAG, "IO Exception while appending file with BufferedWriter");
            e.printStackTrace();
        }
    }

    public static void closeBufferedWriter(){
        try {
            mBufferedWriter.close();
        } catch (IOException e) {
            Log.e(TAG, "IO Exception while closing the BufferedWriter");
            e.printStackTrace();
        }
    }
}
