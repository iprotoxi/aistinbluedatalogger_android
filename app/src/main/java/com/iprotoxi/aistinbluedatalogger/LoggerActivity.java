package com.iprotoxi.aistinbluedatalogger;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class LoggerActivity extends AppCompatActivity {

    private static final String TAG = LoggerActivity.class.getSimpleName();

    public static final String CONNECTED_DEVICE_NAME = "CONNECTED_DEVICE_NAME";
    public static final String CONNECTED_DEVICE_ADDRESS = "CONNECTED_DEVICE_ADDRESS";

    private String mConnectedDeviceName;
    private String mConnectedDeviceAddress;

    private boolean mConnected = false;
    private boolean mLogging = false;

    private BlueService mBlueService;

    private ListView mListView;
    private ArrayAdapter mListAdapter;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    private SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");

    private PowerManager.WakeLock mWakeLock;

    private boolean DEBUG = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_logger);

        //show app logo on action bar
        try {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setLogo(R.mipmap.aistin_eye);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
        } catch (Exception ex) {
            Log.w(TAG, ex);
        }

        //get the connected device name and address
        final Intent intent = getIntent();
        mConnectedDeviceName = intent.getStringExtra(CONNECTED_DEVICE_NAME);
        mConnectedDeviceAddress = intent.getStringExtra(CONNECTED_DEVICE_ADDRESS);

        //bind and start blue service
        Intent gattServiceIntent = new Intent(this, BlueService.class);
        bindService(gattServiceIntent, mBlueServiceConnection, BIND_AUTO_CREATE);

        //set up data view
        mListView = (ListView) findViewById(R.id.listView_data);
        mListAdapter = new ArrayAdapter<>(this, R.layout.list_item_message);
        mListView.setAdapter(mListAdapter);
        mListView.setDivider(null);

        //the not-preferred-way to use wake lock
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "AistinBlueDataLoggerWakelock");
        mWakeLock.acquire();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattStatusReceiver, gattActionIntentFilter());
        if (mBlueService != null){
            final boolean result = mBlueService.connect(mConnectedDeviceAddress);
            if (DEBUG) Log.d(TAG, "Request result for connect: " + result);
        }

        //TODO: Android 6.0 run-time permission requests
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattStatusReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mBlueServiceConnection);
        mBlueService = null;
        //release wakelock
        mWakeLock.release();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logger, menu);

        if (mConnected) {
            menu.findItem(R.id.menu_item_bt_on).setVisible(true);
            menu.findItem(R.id.menu_item_bt_off).setVisible(false);
            menu.findItem(R.id.menu_item_connected).setVisible(true);
            menu.findItem(R.id.menu_item_disconnected).setVisible(false);
        } else {
            menu.findItem(R.id.menu_item_bt_on).setVisible(false);
            menu.findItem(R.id.menu_item_bt_off).setVisible(true);
            menu.findItem(R.id.menu_item_connected).setVisible(false);
            menu.findItem(R.id.menu_item_disconnected).setVisible(true);
        }

        if (mLogging) {
            menu.findItem(R.id.menu_item_log_on).setVisible(true);
            menu.findItem(R.id.menu_item_log_off).setVisible(false);
        } else {
            menu.findItem(R.id.menu_item_log_on).setVisible(false);
            menu.findItem(R.id.menu_item_log_off).setVisible(true);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_bt_on:
                if (DEBUG) Log.d(TAG, "menu_item_bt_on clicked");
                invalidateOptionsMenu();
                break;
            case R.id.menu_item_bt_off:
                if (DEBUG) Log.d(TAG, "menu_item_bt_off clicked");
                invalidateOptionsMenu();
                break;

            case R.id.menu_item_connected:
                if (DEBUG) Log.d(TAG, "menu_item_connected clicked  -> disconnect");
                if (mLogging) stopLogging();
                mBlueService.disconnect();
                mConnected = false;
                invalidateOptionsMenu();
                this.finish(); //go back to scanning
                break;
            case R.id.menu_item_disconnected:
                if (DEBUG) Log.d(TAG, "menu_item_disconnected clicked -> connect?");
                invalidateOptionsMenu();
                break;

            //TODO: Log menu button functionality for custom file name setup
            case R.id.menu_item_log_on:
                if (DEBUG) Log.d(TAG, "menu_item_log_on -> stop logging");
                if (mLogging) {
                    //disable notifications to prevent getting more data from dewice
                    mBlueService.setCharacteristicNotificationNRFUartTX(false);
                    mListAdapter.add(time.format(new Date()) + " |  Notifications disabled");
                    stopLogging();
                } else {
                }
                invalidateOptionsMenu();
                break;

            case R.id.menu_item_log_off:
                if (DEBUG) Log.d(TAG, "menu_item_log_off -> start logging");
                if (mConnected) {
                    if (!mLogging) startLogging();
                }
                invalidateOptionsMenu();
                break;
            default:
                Log.w(TAG, "unrecognized menu item action");
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private final ServiceConnection mBlueServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mBlueService = ((BlueService.LocalBinder) service).getService();
            if (DEBUG) Log.d(TAG, "onServiceConnected");
            if (!mBlueService.initialize()) {
                Log.e(TAG, "Cannot initialize Bluetooth");
                finish();
            }
            mBlueService.connect(mConnectedDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBlueService = null;
            if (DEBUG) Log.d(TAG, "onServiceDisconnected");
        }
    };

    private final BroadcastReceiver mGattStatusReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            switch (action) {
                case BlueService.INTENT_ACTION_GATT_CONNECTED:
                    if (DEBUG) Log.d(TAG, "BroadcastReceiver INTENT_ACTION_GATT_CONNECTED");
                    mConnected = true;
                    invalidateOptionsMenu();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mListAdapter.add(time.format(new Date()) + " |  Today is: " + date.format(new Date()));
                            mListAdapter.add(time.format(new Date()) + " |  Connected device:");
                            mListAdapter.add(time.format(new Date()) + " |  " + mConnectedDeviceName + " [" + mConnectedDeviceAddress + "]");
                        }
                    });
                    break;

                case BlueService.INTENT_ACTION_GATT_DISCONNECTED:
                    if (DEBUG) Log.d(TAG, "BroadcastReceiver INTENT_ACTION_GATT_DISCONNECTED");
                    mConnected = false;
                    invalidateOptionsMenu();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mListAdapter.add(time.format(new Date()) + " |  Device disconnected");
                            mListView.smoothScrollToPosition(mListAdapter.getCount()-1);
                            if (mLogging) {
                                stopLogging();
                            }
                        }
                    });
                    break;

                case BlueService.INTENT_ACTION_GATT_SERVICES_DISCOVERED:
                    if (DEBUG) Log.d(TAG, "BroadcastReceiver INTENT_ACTION_GATT_SERVICES_DISCOVERED");
                    //mBlueService.setCharacteristicNotificationNRFUartTX();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mListAdapter.add(time.format(new Date()) + " |  Ready to start logging");
                            mListView.smoothScrollToPosition(mListAdapter.getCount()-1);
                        }
                    });
                    break;

                case BlueService.INTENT_ACTION_DATA_AVAILABLE:
                    if (DEBUG) Log.d(TAG, "INTENT_ACTION_DATA_AVAILABLE");
                /*
                    final byte[] rawSensorData = intent.getByteArrayExtra(BlueService.INTENT_ACTION_EXTRA_DATA);
                    Log.d(TAG, "" + AistinBlueBoard.ConvertByteArrayToHexString(rawSensorData));
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mLogging) {
                                //mListAdapter.add(message);
                                //mListView.smoothScrollToPosition(mListAdapter.getCount() - 1);
                            } else {
                                //mListAdapter.add(message);
                                //mListAdapter.add(sdf.format(new Date()) + " |  Data message not logged");
                            }
                        }
                    });*/
                    break;

                case BlueService.INTENT_DEVICE_DOES_NOT_SUPPORT_UART:
                    if (DEBUG) Log.d(TAG, "BroadcastReceiver INTENT_DEVICE_DOES_NOT_SUPPORT_UART");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mListAdapter.add(time.format(new Date()) + " |  Device does not support nRF UART.");
                            mListAdapter.add(time.format(new Date()) + " |  Disconnecting...");
                            mListView.smoothScrollToPosition(mListAdapter.getCount() - 1);
                        }
                    });
                    mBlueService.disconnect();
                    break;
            }
        }
    };

    private static IntentFilter gattActionIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BlueService.INTENT_ACTION_DATA_AVAILABLE);
        intentFilter.addAction(BlueService.INTENT_ACTION_EXTRA_DATA);
        intentFilter.addAction(BlueService.INTENT_ACTION_GATT_CONNECTED);
        intentFilter.addAction(BlueService.INTENT_ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BlueService.INTENT_ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BlueService.INTENT_DEVICE_DOES_NOT_SUPPORT_UART);
        return intentFilter;
    }

    /* Returns the default name for a new log file */
    private String getDefaultFileName() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
        String timestamp = sdf.format(Calendar.getInstance().getTime());
        return "log-" + timestamp + ".csv";
    }

    /* Closes the text writer to stop lopping */
    private void stopLogging(){
        setTimestampToLogEnd();
        BlueService.closeBufferedWriter();
        mLogging = false;
        mListAdapter.add(time.format(new Date()) + " |  Stopped logging");
        mListView.smoothScrollToPosition(mListAdapter.getCount() - 1);
    }

    /* Creates a new log file, enables notifications from device & starts logging */
    private void startLogging() {
        mLogging = true;
        //create log folder and new log file
        //if there is no SD card, create a new directory object to make directory on the device
        String mFileName = getDefaultFileName();
        if (Environment.getExternalStorageState() == null) {
            //create new file directory object
            File directory = new File(Environment.getDataDirectory() + "/AistinBlueLogs/");
            // if no directory exists, create new directory
            if (!directory.exists()) {
                directory.mkdir();
            }
            BlueService.setupBufferedWriter(mFileName, directory);
        } // if phone DOES have an SD card
        else if (Environment.getExternalStorageState() != null) {
            // search for directory on SD card
            File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/AistinBlueLogs/");
            // if no directory exists, create new directory
            if (!directory.exists()) {
                directory.mkdir();
            }
            BlueService.setupBufferedWriter(mFileName, directory);
        }// end of SD card checking
        mListAdapter.add("____________________________________________");
        mListAdapter.add(time.format(new Date()) + " |  New log file:");
        mListAdapter.add(time.format(new Date()) + " |  " + mFileName);
        mListAdapter.add(time.format(new Date()) + " |  Data log format:");
        mListAdapter.add(time.format(new Date()) + " |  " + AistinBlueBoard.DATA_LOG_FORMAT);
        //mListAdapter.add(sdf.format(new Date()) + " |  Time sequence coefficient (ms): " + AistinBlueBoard.TIME_SEQUENCE_COEFFICIENT);

        //create file writer
        BlueService.createBufferedWriter();
        setLogHeader();
        //enable notifications to get data
        mListAdapter.add(time.format(new Date()) + " |  Notifications enabled");
        mListAdapter.add(time.format(new Date()) + " |  Started logging...");
        mListView.smoothScrollToPosition(mListAdapter.getCount() - 1);
        mBlueService.setCharacteristicNotificationNRFUartTX(true);
    }

    private void setLogHeader() {
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = sdf.format(Calendar.getInstance().getTime());
        BlueService.appendFile(timestamp + ";Aistin Blue Data Log;(ver.1.4)\n" +
                                            AistinBlueBoard.DATA_LOG_FORMAT + "\n");
    }

    private void setTimestampToLogEnd() {
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        BlueService.appendFile("\n"+ sdf.format(Calendar.getInstance().getTime()) + "\n");
    }
}
